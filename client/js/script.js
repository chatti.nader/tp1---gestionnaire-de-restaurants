window.onload = init;

function init() {
    new Vue({
        el: "#app",
        data: {
            restaurants: [],
            nbRestaurants: 0,
            nbPagesDeResultats:0,
            name: '',
            cuisine: '',
            page: 0,
            pagesize: 10,
            nomRecherche: "",
            id: '',
            messageAffichage: '',
            name_modif: '',
            cuisine_modif: '',
            id_modif: '',
            ajout_form: false,
            modif_form: false
        },
        mounted() {
            console.log("AVANT AFFICHAGE");
            this.getRestaurantsFromServer();
        },
        methods: {
            // ----------------------------------Récupération de la liste restaurants depuis le serveur----------------------------------
            getRestaurantsFromServer() {
                let url = "http://localhost:8080/api/restaurants?page=" +
                    this.page +
                    "&pagesize=" + this.pagesize +
                    "&name=" + this.nomRecherche;
                fetch(url)
                    .then((responseJS) => {
                        responseJS.json()
                            .then((responseJS) => {
                                // Maintenant res est un vrai objet JavaScript
                                console.log("restaurants récupérés");
                                this.restaurants = responseJS.data;
                                this.nbRestaurants = responseJS.count;
                                this.nbPagesDeResultats = Math.ceil(this.nbRestaurants/this.pagesize);
                            });
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            },

            // ----------------------------------Ajout d'un restaurant----------------------------------
            formAjout() {
                // Afficher le formulaire d'ajout et cacher le formulaire de modification
                this.ajout_form = true;
                this.modif_form = false;
            },

            // ----------------------------------Envoie des données à ajouter à la BD----------------------------------
            ajouterRestaurant(event) {
                // eviter le comportement par defaut
                event.preventDefault();

                // On recupère le formulaire
                let form = event.target;

                // On recupere les données du formulaire
                let dataFormulaire = new FormData(form);

                // On envoie une requête POST au serveur
                let url = "http://localhost:8080/api/restaurants";

                // PopUp "Swal" de confirmation d'ajout
                // Si oui envoie à la BD + Message 'success'
                // Si non abondon + Message 'error'
                Swal({
                    title: 'Ajouter le restaurant ?',
                    text: '',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#ddd',
                    cancelButtonColor: '#555',
                    confirmButtonText: 'Ajouter !',
                    cancelButtonText: 'Annuler !'
                }).then((result) => {
                    if (result.value) {
                        fetch(url, {
                            method:"POST",
                            body: dataFormulaire
                        })
                            .then((responseJS) => {
                                responseJS.json()
                                    .then((responseJS) => {
                                        // Maintenant res est un vrai objet JavaScript
                                        console.log(responseJS.msg);
                                        this.getRestaurantsFromServer();
                                    });
                                Swal({
                                    type: 'success',
                                    title: 'Restaurant ajouté !',
                                    showConfirmButton: false,
                                    timer: 800
                                });
                                // Remise des champs du formulaire à zéro
                                this.name='';
                                this.cuisine='';

                                // Cacher le formulaire d'ajout
                                this.ajout_form = false;
                            })
                            .catch(function (err) {
                                console.log(err);
                                Swal({
                                    type: 'error',
                                    title: 'Erreur !',
                                    showConfirmButton: false,
                                    timer: 800
                                });
                            });
                    }
                });
            },

            // ----------------------------------Modification d'un restaurant----------------------------------
            modifierRestaurant(r) {
                // Récupération des données du restaurant sélectionné
                this.id_modif=r._id;
                this.name_modif=r.name;
                this.cuisine_modif = r.cuisine;

                // Afficher le formulaire de modification et cacher le formulaire d'ajout
                this.ajout_form = false;
                this.modif_form = true;
            },

            // ----------------------------------Envoie de la modification à la BD----------------------------------
            envoiModificationRestaurant(event){
                event.preventDefault();
                let form = event.target;
                let donneesFormulaire = new FormData(event.target);
                let id = this.id_modif;
                let url = "http://localhost:8080/api/restaurants/" + id;

                // PopUp "Swal" de confirmation de modification
                // Si oui envoi à la BD + Message 'success'
                // Si non rien abondon + Message 'error'
                Swal({
                    title: 'Confirmer les modifications ?',
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#ddd',
                    cancelButtonColor: '#555',
                    confirmButtonText: 'Modifier !',
                    cancelButtonText: 'Annuler !'
                }).then((result) => {
                    if (result.value) {
                        fetch(url, {
                            method: "PUT",
                            body: donneesFormulaire
                        }).then((responseJSON) => {
                            responseJSON.json()
                                .then((res) => {
                                    console.log(res.msg);
                                    this.getRestaurantsFromServer();
                                });
                        });
                        Swal({
                            type: 'success',
                            title: 'Modification éffectuée !',
                            showConfirmButton: false,
                            timer: 800
                        });

                        // Remise des champs du formulaire à zéro
                        this.id_modif='';
                        this.name='';
                        this.cuisine='';

                        // Chacher le formulaire de modification
                        this.modif_form = false;
                    } else
                        Swal({
                            type: 'error',
                            title: 'Modification abandonnée !',
                            showConfirmButton: false,
                            timer: 800
                        }).catch(function (err) {
                            console.log("Une Erreur est produite " + err);
                        });
                });
            },

            // ----------------------------------Suppression d'un restaurant----------------------------------
            supprimerRestaurant(index) {
                // PopUp "Swal" de confiration de suppression
                // Si oui envoie à la BD + Merssage 'success'
                // Si non abondon + Message 'error'
                Swal({
                    title: 'Supprimer ce restaurant ?',
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#ddd',
                    cancelButtonColor: '#555',
                    confirmButtonText: 'Supprimer !',
                    cancelButtonText: 'Annuler !'
                }).then((result) => {
                    if (result.value) {
                        this.envoieRequeteFetchDelete(index);
                        console.log("l'index à supprimer: " +index);
                        Swal({
                            type: 'success',
                            title: 'Suppression éffectuée !',
                            showConfirmButton: false,
                            timer: 800
                        });
                    } else
                        Swal({
                            type: 'error',
                            title: 'Suppression abandonnée !',
                            showConfirmButton: false,
                            timer: 800
                        });
                });
            },

            // ----------------------------------Envoie de la requête de suppression à la BD----------------------------------
            envoieRequeteFetchDelete(id) {
                let url = "http://localhost:8080/api/restaurants/" + id;
                fetch(url, {
                    method: "DELETE",
                })
                    .then((responseJSON) => {
                        responseJSON.json()
                            .then((res) => {
                                console.log("Restaurant supprimé");
                                this.getRestaurantsFromServer();
                                this.messageAffichage="Suppression Réussite";
                            });
                    })
                    .catch(function (err) {
                        console.log("Une Erreur est produite " +err);
                    });
            },

            // ----------------------------------Appel de la dernière page----------------------------------
            premierePage() {
                this.page = 0;
                this.getRestaurantsFromServer()
            },

            // ----------------------------------Appel de la page suivante----------------------------------
            pageSuivante() {
                this.page++;
                this.getRestaurantsFromServer();
            },

            // ----------------------------------Appel de la page précédente----------------------------------
            pagePrecedente() {
                this.page--;
                this.getRestaurantsFromServer();
            },

            // ----------------------------------Appel de la dernière page----------------------------------
            dernierePage() {
                this.page = (this.nbPagesDeResultats - 1);
                this.getRestaurantsFromServer();
            },

            // ----------------------------------Appel de la page demandée par les boutons présents (Cas de +/- 3 pages)----------------------------------
            selectionPage(n) {
                this.page = this.page + n;
                this.getRestaurantsFromServer()
            },

            // ----------------------------------Reset des formulaires----------------------------------
            resetform() {
                Swal({
                    title: 'Abandonner les modifications ?',
                    text: '',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#ddd',
                    cancelButtonColor: '#555',
                    confirmButtonText: 'Abandonner !',
                    cancelButtonText: 'Continuer !'
                }).then((result) => {
                    if (result.value) {
                        // Cacher les formulaires ajout et modification
                        this.ajout_form = false;
                        this.modif_form = false;
                        Swal({
                            type: 'warning',
                            title: 'Abondon !',
                            showConfirmButton: false,
                            timer: 800
                        });
                    }
                });
            }
        }
    })
}